# Anynines Applicant Homework 

This repository contains two kinds of homework for applicants.

## Bosh Homework

Template for a bosh based homework.

## Ruby App Homework

Template for a ruby app homework.

## Running Tests Task 1 ruby

```bash
cd ruby
```

Before running any Ruby tests, ensure that you have the necessary environment set up. Before running any Ruby tests, ensure that you have the necessary environment set up. This requires installing [Docker](https://www.docker.com/).


```bash
docker-compose up
```

```bash
bundle exec rspec spec/
```
## Running Tests Task 2 Bosh

Before running the Bosh tests, ensure that you have [Bosh](https://bosh.io/docs) installed.

> To install Bosh, follow the instructions on the [official Bosh website](https://bosh.io/docs/cli-v2-install/).

```bash
cd /bosh/ruby-app-release/unit-tests/
```

```bash
bundle exec rspec spec/
```
